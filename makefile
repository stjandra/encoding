SRCDIR = src
OBJDIR = obj
BINDIR = bin

CC = gcc

CFLAGS = -O3 -Wall

all: $(BINDIR)/bwt $(BINDIR)/bwtd $(BINDIR)/mtf $(BINDIR)/mtfd \
	 $(BINDIR)/rle $(BINDIR)/rled

$(OBJDIR)/%.o: $(SRCDIR)/%.c
	$(CC) $(CFLAGS) -c $< -o $@

$(BINDIR)/bwt: $(OBJDIR)/bwt.o $(OBJDIR)/read.o
	$(CC) -o $@ $^

$(BINDIR)/bwtd: $(OBJDIR)/bwtd.o $(OBJDIR)/read.o
	$(CC) -o $@ $^

$(BINDIR)/mtf: $(OBJDIR)/mtf.o $(OBJDIR)/read.o $(OBJDIR)/array.o
	$(CC) -o $@ $^

$(BINDIR)/mtfd: $(OBJDIR)/mtfd.o $(OBJDIR)/read.o $(OBJDIR)/array.o
	$(CC) -o $@ $^

$(BINDIR)/rle: $(OBJDIR)/rle.o $(OBJDIR)/read.o
	$(CC) -o $@ $^

$(BINDIR)/rled: $(OBJDIR)/rled.o $(OBJDIR)/read.o
	$(CC) -o $@ $^

clean:
	rm $(OBJDIR)/*.o $(BINDIR)/bwt $(BINDIR)/bwtd $(BINDIR)/mtf \
		$(BINDIR)/mtfd $(BINDIR)/rle $(BINDIR)/rled