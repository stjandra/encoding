# encoding

This repository contains the C implementation of three encoding techniques: Burrows-Wheeler transform, Move-to-front transform, and Run-length encoding.

## Run Instructions

### Compiling

Compile with `make`.

This will generate six executables in the `bin` directory:

| Executable | Algorithm                       |
|------------|---------------------------------|
| bwt        | Burrows-Wheeler transform (BWT) |
| bwtd       | Reverse BWT                     |
| mtf        | Move-to-front transform (MTF)   |
| mtfd       | Reverse MTF                     |
| rle        | Run-length encoding (RLE)       |
| rled       | Reverse RLE                     |

### Applying BWT

Go to the `bin` directory:

`$ cd bin/`

To apply BWT to file `foo.txt` and save the result to file `bwt.out`:

`$ bwt < foo.txt > bwt.out`

To get the original file back:

`$ bwtd < bwt.out > foo_copy.txt`

### Applying All Three Encoding Techniques

To apply BWT, MTF, and RLE in that order, to file `foo.txt` and save the result to file `rle.out`:

`$ bwt < foo.txt | mtf | rle > rle.out`

To get the original file back:

`$ rled < rle.out | mtfd | bwtd > foo_copy.txt`

## Program

### bwt

Since we need a special end-of-file (EOF) character for BWT, each byte is converted to short integer (2 bytes) and added with a binary 1. The EOF character can then be represented by 0 in short integer and is guaranteed to be unique.

For example, let '$' denotes the EOF character. The BWT of the string "banana$" is "annb$aa". Since each byte is added with binary 1, it becomes "booc$bb" where each character is represented using two bytes and '$' has the value 0.


### mtf and rle

Since each byte is represented as two bytes (see previous section), `mtf` and `rle` read the given input as a sequence of short integers.