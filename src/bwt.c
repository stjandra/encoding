#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "read.h"

int cmp (const void* a, const void * b);
short get_next(int curr);
short get_last(int start_at);

short* s;
/* If int is represented using 4 bytes, its maximum value is 2,147,483,647.
 * Hence, the maximum file size supported is 2,147,483,647 bytes (2.1 GB). */
int len;
int* prefixes;

int main(int argc, char* argv[]) {
	int i;

	// read from stdin
	s = read_binary(&len);

	// actually, we can use "prefixes" array to store result, maybe in the future

	short* result = malloc(sizeof(short) * len);
	if(result == NULL) {
		fprintf(stderr, "Failed to allocate result.");
		exit(1);
	}

	// prefixes array stores the index of prefix
	prefixes = malloc(sizeof(int) * len);
	if(prefixes == NULL) {
		fprintf(stderr, "Failed to allocate prefixes.");
		exit(1);
	}
	for (i=0; i<len; i++) {
		prefixes[i] = i;
	}

	qsort(prefixes, len, sizeof(int), cmp);

	 // get the last element from the sorted prefixes
	for (i=0; i<len; i++) {
		result[i] = get_last(prefixes[i]);
	}

	// write result to stdout
	fwrite(result, sizeof(short), len, stdout);

	free(s);
	free(result);
	free(prefixes);
}

/* Returns positive if prefix at index a is greater than prefix at index b. */
int cmp (const void* a, const void* b) {
	int index_a = *(int*)a;
	int index_b = *(int*)b;

	/* get value at index a and b */
	short value_a = s[index_a];
	short value_b = s[index_b];

	int result = value_a - value_b;

	while (result == 0) {
	   value_a = get_next(index_a);
	   value_b = get_next(index_b);
	   // index_a and index_b will never get greater than (len-1)
	   // because the value at index (len-1) is unique (hence result will not be 0)
	   index_a++;
	   index_b++;
	   result = value_a - value_b;
	}

	return result;
}


/* Returns the next character after index curr.
 * When the last character is reached, wraps around to beginning. */
short get_next(int curr) {
	if (curr == len-1) {
		return s[0];
	}
	return s[curr+1];
}

/* Returns the last character of the given prefix. */
short get_last(int start_at) {
	if (start_at == 0) {
		return s[len-1];
	}
	return s[start_at-1];
}

