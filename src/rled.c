#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "read.h"

#define SIZE 1024

short* decode(short* s, int len_s, int* len_result);

int main(int argc, char* argv[]) {
	short* s;
	int len_s = 0;

	// read from stdin
	s = read_bwt(&len_s);

	short* result;
	int len_result;

	result = decode(s, len_s, &len_result);

	fwrite(result, sizeof(short), len_result, stdout);

	free(s);
	free(result);

	return 0;
}


/* Performs reverse run-length decoding. */
short* decode(short* s, int len_s, int* len_result) {
	int curr_size = SIZE;

	short* result = malloc(sizeof(short) * curr_size);
	if(!result) {
		fprintf(stderr, "Failed to allocate result.");
		exit(1);
	}

	*len_result = 0;
	short freq;
	short val;
	for (int i=0; i<len_s; i+=2) {
		freq = s[i];
		val = s[i+1];

		// TODO delete
//		if (freq <= 0) {
//			printf("at %d freq %d\n", i, freq);
//		}

		// append val freq times
		for (int j=0; j<freq; j++) {
			if (*len_result >= curr_size) {
				// allocate more memory
				curr_size *= 2;
				result = (short*) realloc(result, sizeof(short)*(curr_size));
			}

			result[*len_result] = val;
			*len_result += 1;
		}
	}

	return result;
}




