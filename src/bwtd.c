#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "read.h"

int get_index_terminating_null();

short* s;
int len=0;
unsigned char* result;

int* num_occurrences;
// 257 because there are 256 different values for a byte and +1 for the special terminating symbol
int num_less_than[257];

int main(int argc, char* argv[]) {
	int i;
	int index_terminating_null;

	// read from stdin
	s = read_bwt(&len);

	num_occurrences = malloc(sizeof(int) * (len));
	if(num_occurrences == NULL) {
		fprintf(stderr, "Failed to allocate num_occurences.");
		exit(1);
	}

	// TODO use memset
	// initialize the two arrays with 0s
	for (i=0; i<len; i++) {
		num_occurrences[i] = 0;
		if (i < 257) {
			num_less_than[i] = 0;
		}
	}

	// count the number of occurrences of current symbol before current position 
	for (i=0; i<len; i++) {
		// initially, use num_less_than to store current count 
		num_occurrences[i] += num_less_than[s[i]];
		num_less_than[s[i]] += 1;
	}

	// count the number of characters that are smaller than the current symbol 
	int prev_less_than_count = 0;
	int curr_count;
	for (i=0; i<257;i++) {
		curr_count = num_less_than[i];
		num_less_than[i] = prev_less_than_count;
		prev_less_than_count += curr_count;
	}

	result = malloc(sizeof(char) * (len));
	if(result == NULL) {
		fprintf(stderr, "Failed to allocate result.");
		exit(1);
	}

	index_terminating_null = get_index_terminating_null();

	// reverse bwt
	int num_occurr = 0;
	int num_lt = 0;
	int index = index_terminating_null;
	result[len-1] = (unsigned char) s[index_terminating_null];
	for (i=len-2; i>=0; i--) {
		num_occurr = num_occurrences[index];
		num_lt = num_less_than[s[index]];
		index = num_occurr + num_lt;
		// -1 to get the original value 
		result[i] = (unsigned char) (s[index] - 1);
	}

	// -1 to ignore terminating 0 
	fwrite(result, sizeof(unsigned char), len-1, stdout);

	free(result);
	free(s);
	free(num_occurrences);

	return 0;
}


/* Gets the index of the terminating 0. */
int get_index_terminating_null() {
	for (int i=0; i<len; i++) {
		if (s[i] == 0) {
			return i;
		}
	}

	fprintf(stderr, "Error, terminating 0 not found.");
	exit(1);
}

