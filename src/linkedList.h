typedef struct node_t node_t;
typedef short value_t;

struct node_t {
   value_t value;
   node_t* next;
};

node_t* initialize_queue();
node_t* move_value_to_front(node_t* head, value_t v, int* index);
node_t* move_nth_to_front(node_t* head, int index, value_t* v);
node_t* get_nth_node(node_t* head, int n, node_t** prev);
void free_linked_list(node_t* head);
void print_linked_list(node_t* head);
