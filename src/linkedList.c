#include <stdio.h>
#include <stdlib.h>
#include "linkedList.h"

node_t* initialize_node(value_t value);

node_t* initialize_node(value_t value) {
	node_t* curr = malloc (sizeof (node_t));
	if (curr == NULL) {
		fprintf(stderr, "Failed to initialize linked list.");
		exit(1);
	}

	curr->value = value;
	curr->next = NULL;

	return curr;
}

/* Initializes queue to have values from 0 to 256. */
node_t* initialize_queue() {
	node_t* head = initialize_node(0);
	node_t* prev = head;
	node_t* curr;

	// write nodes for 1 to 256
	for (int i=1; i<=256; i++) {
		curr = initialize_node(i);
		prev->next = curr;	// link previous node to current node
		prev = curr;
	}

	// last node
	prev->next = NULL;

	return head;
}


/* Get the index of v in queue and move it to the front. */
node_t* move_value_to_front(node_t* head, value_t v, int* index) {
	node_t* prev = NULL;
	node_t* curr = head;
	int i = 0;

	while (curr != NULL) {
		if (curr->value == v) {
			// found it
			if (prev == NULL) {
				// v is at head
				*index = i;
				return head;
			}
			else {
				// move curr to front
				prev->next = curr->next;
				curr->next = head;
			}
			*index = i;
			return curr;
		}

		// v is not in curr
		prev = curr;
		curr = curr->next;
		i++;
	}

	// value not found 
	fprintf(stderr, "The list does not have the specified value.");
	exit(1);
}

/* Get the value of the given index and move it to the front. */
node_t* move_nth_to_front(node_t* head, int n, value_t* v) {
	node_t* prev;
	node_t* curr;

	if (n == 0) {
		// easy
		*v = head->value;
		return head;
	}

	// now that n cannot be 0 
	// get nth node and set v to its value 
	curr = get_nth_node(head, n, &prev);
	*v = curr->value;

	// move it to the front 
	prev->next = curr->next;
	curr->next = head;

	return curr;
}

node_t* get_nth_node(node_t* head, int n, node_t** prev) {
	node_t* curr=head;

	if (n==0) {
		*prev = NULL;
		return head;
	}

	for (int i=0; i<n; i++) {
		if (curr->next == NULL) {
			fprintf(stderr, "n is too large.");
			exit(1);
		}
		*prev = curr;
		curr = curr->next;
	}

	return curr;
}

void free_linked_list(node_t* head) {
	node_t* curr = head;
	node_t* next = head->next;

	while (curr != NULL) {
		next = curr->next;
		free(curr);
		curr = next;
	}
}


// for debugging
void print_linked_list(node_t* head) {
	node_t* curr=head;

	while (curr != NULL) {
		printf("%d, ", curr->value);
		curr = curr->next;
	}
}



