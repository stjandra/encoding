#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "array.h"

#define LEN	257

/* Initializes queue to have values from 0 to 256. */
value_t* initialize_queue() {
	value_t* queue = malloc (sizeof (value_t)*LEN);
	if (queue == NULL) {
		fprintf(stderr, "Failed to initialize array.");
		exit(1);
	}

	for (int i=0; i<LEN; i++) {
		queue[i] = i;
	}
	return queue;
}


/* Get the index of v in queue and move it to index 0. */
value_t* move_value_to_front(value_t* queue, value_t v, int* index) {

	if (queue[0] == v) {
		// special case if v is at front
		*index = 0;
		return queue;
	}

	for (int i=1; i<LEN; i++) {
		if (queue[i] == v) {
			// found it
			// move preceding elements by 1 to the right
			memmove(queue+1, queue, i*sizeof(*queue));
			// move v to index 0
			queue[0] = v;

			*index = i;
			return queue;
		}
	}

	// value not found
	fprintf(stderr, "The array does not have the specified value");
	exit(1);
}


/* Get the value of the given index and move it to index 0. */
value_t* move_nth_to_front(value_t* queue, int index, value_t* v) {

	*v = queue[index];

	if (index == 0) {
		// special case if index is 0
		return queue;
	}

	// move preceding elements by 1 to the right
	memmove(queue+1, queue, index*sizeof(*queue));
	// move v to index 0
	queue[0] = *v;

	return queue;
}


// for debugging
void print_array(value_t* queue) {
	for (int i=0; i<LEN; i++) {
		printf("%d, ", queue[i]);
	}
}









