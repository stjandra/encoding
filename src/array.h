typedef short value_t;

value_t* initialize_queue();
value_t* move_value_to_front(value_t* queue, value_t v, int* index);
value_t* move_nth_to_front(value_t* queue, int index, value_t* v);
void print_array(value_t* queue);
