#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define BUFF_SIZE 524288//262144


short* process_buffer_binary(unsigned char* buff, int buff_len, short* result, int* result_len);
short* process_buffer_bwt(short* buff, int buff_len, short* result, int* result_len);

/* Reads from stdin.
 * Adds 1 to each byte and converts each byte to short integer (2 bytes).
 * Converts each byte to short integer (2 bytes) and adds 1 to each.
 * Puts 0 (short) at the end.
 * Returns array of short. */
short* read_binary(int* len) {
	unsigned char* buff = (unsigned char*) malloc(sizeof(unsigned char)*BUFF_SIZE);
	if (!buff) {
		fprintf(stderr, "Failed to allocate buff.");
		exit(1);
	}

	short* result=NULL;
	int bytes_read;
	*len = 0;

	// read from stdin until end of file
	while((bytes_read=fread(buff, sizeof(unsigned char), BUFF_SIZE, stdin)) > 0){
		result = process_buffer_binary(buff, bytes_read, result, len);
	}

	// add 0 at the end
	result[*len] = (short)'\0';
	*len += 1;

	return result;
}


/* Adds 1 to every element in buff and appends them to result. */
short* process_buffer_binary(unsigned char* buff, int buff_len, short* result, int* result_len) {
	// +1 for terminating 0 at the end
	result = (short*) realloc(result, sizeof(short)*(buff_len+*result_len+1));

	for (int i=0; i<buff_len; i++) {
		result[i+*result_len] = (short)buff[i] + 1;
	}
	*result_len += buff_len;

	return result;
}


/* Reads from stdin to array of short. */
short* read_bwt(int* len) {
	short* buff = (short*) malloc(sizeof(short)*BUFF_SIZE);
	if (!buff) {
		fprintf(stderr, "Failed to allocate buff.");
		exit(1);
	}

	short* result=NULL;
	int bytes_read;
	*len = 0;

	// read from stdin until end of file
	while((bytes_read=fread(buff, sizeof(short), BUFF_SIZE, stdin)) > 0){
		result = process_buffer_bwt(buff, bytes_read, result, len);
	}

	return result;
}


/* Appends buff to result */
short* process_buffer_bwt(short* buff, int buff_len, short* result, int* result_len) {
	result = (short*) realloc(result, sizeof(short)*(buff_len+*result_len));

	for (int i=0; i<buff_len; i++) {
		result[i+*result_len] = buff[i];
	}
	*result_len += buff_len;

	return result;
}

