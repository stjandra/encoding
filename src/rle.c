#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "read.h"

#define SIZE 1024
#define MAX_SHORT 32767

short* encode(short* s, int len_s, int* len_result);

int main(int argc, char* argv[]) {
	short* s;
	int len_s = 0;

	// read from stdin
	s = read_bwt(&len_s);

	short* result; // frequency and value interleave
	int len_result;

	result = encode(s, len_s, &len_result);

	fwrite(result, sizeof(short), len_result, stdout);

	free(s);
	free(result);

	return 0;
}


/* Performs run-length encoding.
 * Returns an array of frequency and value from array s. */
short* encode(short* s, int len_s, int* len_result) {
	short* result = malloc(sizeof(short) * len_s * 2);
	if(!result) {
		fprintf(stderr, "Failed to allocate result.");
		exit(1);
	}

	short curr_frequency = 1;
	short curr_value = s[0];
	int len = 0;

	for (int i=1; i<len_s; i++) {
		if (s[i] == curr_value && curr_frequency <= MAX_SHORT-1) {
			curr_frequency++;
			continue;
		}
		else {
			// store current value
			result[len] = curr_frequency;
			result[len+1] = curr_value;
			len+=2;

			// reset
			curr_value = s[i];
			curr_frequency = 1;
		}
	}

	// store cur_value for last value
	result[len] = curr_frequency;
	result[len+1] = curr_value;
	len+=2;

	*len_result = len;
	return result;
}





