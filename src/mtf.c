#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "read.h"
//#include "linkedList.h"
#include "array.h" // uses array instead of linked list

int main(int argc, char* argv[]) {
	short* s;
	int len=0;

	value_t* queue;

	// read from stdin
	s = read_bwt(&len);

	queue = initialize_queue();

	short* result = malloc(sizeof(short) * len);
	if(result == NULL) {
		fprintf(stderr, "Failed to allocate result");
		exit(1);
	}

	int index;
	for (int i=0; i<len; i++) {
		queue = move_value_to_front(queue, s[i], &index);
		result[i] = index;
	}

	// write result to stdout
	fwrite(result, sizeof(short), len, stdout);

	free(s);
	free(result);

	return 0;
}


