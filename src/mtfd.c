#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "read.h"
//#include "linkedList.h"
#include "array.h" // use array instead of linked list

int main(int argc, char* argv[]) {
	short* s;
	int len=0;

	value_t* queue;

	// read from stdin
	s = read_bwt(&len);

	queue = initialize_queue();

	short* result = malloc(sizeof(short) * len);
	if(result == NULL) {
		fprintf(stderr, "Failed to allocate result");
		exit(1);
	}

	short value;
	for (int i=0; i<len; i++) {
		queue = move_nth_to_front(queue, s[i], &value);
		result[i] = value;
	}

	// write result to stdout
	fwrite(result, sizeof(short), len, stdout);

	free(s);
	free(result);

	return 0;
}
